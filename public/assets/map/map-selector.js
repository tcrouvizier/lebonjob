$(document).ready(function () {
    //on masque le select classique
    $("#map-selector").css("display", "none");
    //on ajoute un div #container-map-selector qui contiendra la carte
    $("#map-selector").parent().append("<div id='container-map-selector'></div>");
    //on initie la carte sur cet élément
    var map = new jvm.Map({
        container: $("#container-map-selector"),
        map: 'fr_merc',
        regionsSelectable: false,
        onRegionClick: function (element, code, region) {
            //au clic sur un département
            console.log(code);
            $("#map-selector").val("");//on vide le select
            $("#map-selector option:selected").prop("selected", null);
            $("#map-selector option[value=" + code + "]").prop("selected", true);
            var url = $("#map-selector").find('option:selected').attr('data-url');
            alert(url);
            document.location.href = url;
        }
    });
    //au départ si des options du select sont sélectionné, on les sélectionne sur la carte
    /*$("#map-selector option:selected").each(function () {
        map.setSelectedRegions($(this).val());
    });*/
    $('.geoloc').click(function(){
        alert("Si tu veux implémenter ca, ce n'est pas demandé dans le TP, mais c'est tout à fait possible dans public/assets/map/map-selector.js");
        /* NEED HTTPS or Localhost
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                var infopos = "Position déterminée :\n";
                infopos += "Latitude : "+position.coords.latitude +"\n";
                infopos += "Longitude: "+position.coords.longitude+"\n";
                infopos += "Altitude : "+position.coords.altitude +"\n";
                document.getElementById("infoposition").innerHTML = infopos;
            });
        }
        **/

    });
});


