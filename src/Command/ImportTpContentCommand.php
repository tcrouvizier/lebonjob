<?php
namespace App\Command;

use App\Entity\Department;
use App\Entity\Job;
use App\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class ImportTpContentCommand extends ContainerAwareCommand
{

    protected $em;

    protected function configure()
    {
        $this
            ->setName('tp:import:jobs')
            ->setDescription('Import TP Content')
            ->setHelp('')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $today = new \DateTime();

        $output->writeln([
            '============',
            ' Import departments in DB ...',
            '============',
        ]);
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $depList = $this->departmentsList();
        $depRepo = $em->getRepository('App:Department');
        $jobRepo = $em->getRepository('App:Job');
        $offerRepo = $em->getRepository('App:Offer');
        foreach($depList as $code=>$name)
        {
            $department = $depRepo->findOneBy(array("code"=>$code));
            if(!$department)$department = new Department();
            $department->setName($name);
            $department->setCode($code);
            $em->persist($department);
            $em->flush();
        }
        $output->writeln([
            '============',
            ' Import job and offers in DB ...',
            '============',
        ]);
        if(file_exists('./bin/offers.json'))
        {
            $data = json_decode(file_get_contents('./bin/offers.json'),true);
            $count=0;
            foreach($data['offers'] as $company_jobs)
            {
                foreach($company_jobs as $ref=>$offerArr)
                {
                    $count++;
                    if(isset($offerArr['title']) && isset($offerArr['location'])){

                        $offer = $offerRepo->findOneBy(array('reference'=>$ref));
                        if(!$offer)$offer=new Offer();
                        $job = $jobRepo->findOneBy(array('name'=>$offerArr['title']));
                        if(!$job){
                            $job=new Job();
                            $job->setCode($offerArr['romeCode']);
                            $job->setName($offerArr['title']);
                            $job->setCreatedAt($today);
                            $job->setSticky($count%1000==0);
                            $em->persist($job);

                        }
                        $offer->setJob($job);

                        $departmentCode = substr($offerArr['location']['codePostal'],0,2);
                        $dep = $depRepo->findOneBy(array('code'=>$departmentCode));
                        if(!$dep){
                            $output->writeln([
                                'Department '.$departmentCode.' does not exist in DB'
                            ]);
                        }
                        $offer->setDepartment($dep);
                        $offer->setReference($ref);

                        $offer->setZipcode($offerArr['location']['codePostal']);
                        $offer->setCity($offerArr['location']['libelle']);
                        if(isset($offerArr['location']['latitude'])) {
                            $offer->setLatitude($offerArr['location']['latitude']);
                            $offer->setLongitude($offerArr['location']['longitude']);
                        }
                        $offer->setDescription($offerArr['description']);
                        $offer->setCreatedAt(new \DateTime($offerArr['date']));

                        $offer->setContactEmail('alexneymon+tpcontact'.$ref.'@gmail.com');

                        $em->persist($offer);

                        if($count%50==0)
                        {
                            $em->flush();
                            $output->writeln([
                                $count.' ...'
                            ]);
                        }
                    }


                }
            }
            $output->writeln([
                "DONE !"
            ]);
        }
        else
        {
            $output->writeln([
                '============',
                ' ERROR : Json not found in bin folder',
                '============',
            ]);

        }


    }
    public function departmentsList()
    {
        $depts = array();
        $depts["01"] = "Ain (01)";
        $depts["02"] = "Aisne (02)";
        $depts["03"] = "Allier (03)";
        $depts["04"] = "Alpes de Haute Provence (04)";
        $depts["05"] = "Hautes Alpes (05)";
        $depts["06"] = "Alpes Maritimes (06)";
        $depts["07"] = "Ardèche (07)";
        $depts["08"] = "Ardennes (08)";
        $depts["09"] = "Ariège (09)";
        $depts["10"] = "Aube (10)";
        $depts["11"] = "Aude (11)";
        $depts["12"] = "Aveyron (12)";
        $depts["13"] = "Bouches du Rhône (13)";
        $depts["14"] = "Calvados (14)";
        $depts["15"] = "Cantal (15)";
        $depts["16"] = "Charente (16)";
        $depts["17"] = "Charente Maritime (17)";
        $depts["18"] = "Cher (18)";
        $depts["19"] = "Corrèze (19)";
        $depts["2A"] = "Corse du Sud (2A)";
        $depts["2B"] = "Haute Corse (2B)";
        $depts["21"] = "Côte d'Or (21)";
        $depts["22"] = "Côtes d'Armor (22)";
        $depts["23"] = "Creuse (23)";
        $depts["24"] = "Dordogne (24)";
        $depts["25"] = "Doubs (25)";
        $depts["26"] = "Drôme (26)";
        $depts["27"] = "Eure (27)";
        $depts["28"] = "Eure et Loir (28)";
        $depts["29"] = "Finistère (29)";
        $depts["30"] = "Gard (30)";
        $depts["31"] = "Haute Garonne (31)";
        $depts["32"] = "Gers (32)";
        $depts["33"] = "Gironde (33)";
        $depts["34"] = "Hérault (34)";
        $depts["35"] = "Ille et Vilaine (35)";
        $depts["36"] = "Indre (36)";
        $depts["37"] = "Indre et Loire (37)";
        $depts["38"] = "Isère (38)";
        $depts["39"] = "Jura (39)";
        $depts["40"] = "Landes (40)";
        $depts["41"] = "Loir et Cher (41)";
        $depts["42"] = "Loire (42)";
        $depts["43"] = "Haute Loire (43)";
        $depts["44"] = "Loire Atlantique (44)";
        $depts["45"] = "Loiret (45)";
        $depts["46"] = "Lot (46)";
        $depts["47"] = "Lot et Garonne (47)";
        $depts["48"] = "Lozère (48)";
        $depts["49"] = "Maine et Loire (49)";
        $depts["50"] = "Manche (50)";
        $depts["51"] = "Marne (51)";
        $depts["52"] = "Haute Marne (52)";
        $depts["53"] = "Mayenne (53)";
        $depts["54"] = "Meurthe et Moselle (54)";
        $depts["55"] = "Meuse (55)";
        $depts["56"] = "Morbihan (56)";
        $depts["57"] = "Moselle (57)";
        $depts["58"] = "Nièvre (58)";
        $depts["59"] = "Nord (59)";
        $depts["60"] = "Oise (60)";
        $depts["61"] = "Orne (61)";
        $depts["62"] = "Pas de Calais (62)";
        $depts["63"] = "Puy de Dôme (63)";
        $depts["64"] = "Pyrénées Atlantiques (64)";
        $depts["65"] = "Hautes Pyrénées (65)";
        $depts["66"] = "Pyrénées Orientales (66)";
        $depts["67"] = "Bas Rhin (67)";
        $depts["68"] = "Haut Rhin (68)";
        $depts["69"] = "Rhône (69)";
        $depts["70"] = "Haute Saône (70)";
        $depts["71"] = "Saône et Loire (71)";
        $depts["72"] = "Sarthe (72)";
        $depts["73"] = "Savoie (73)";
        $depts["74"] = "Haute Savoie (74)";
        $depts["75"] = "Paris (75)";
        $depts["76"] = "Seine Maritime (76)";
        $depts["77"] = "Seine et Marne (77)";
        $depts["78"] = "Yvelines (78)";
        $depts["79"] = "Deux Sèvres (79)";
        $depts["80"] = "Somme (80)";
        $depts["81"] = "Tarn (81)";
        $depts["82"] = "Tarn et Garonne (82)";
        $depts["83"] = "Var (83)";
        $depts["84"] = "Vaucluse (84)";
        $depts["85"] = "Vendée (85)";
        $depts["86"] = "Vienne (86)";
        $depts["87"] = "Haute Vienne (87)";
        $depts["88"] = "Vosges (88)";
        $depts["89"] = "Yonne (89)";
        $depts["90"] = "Territoire de Belfort (90)";
        $depts["91"] = "Essonne (91)";
        $depts["92"] = "Hauts de Seine (92)";
        $depts["93"] = "Seine St Denis (93)";
        $depts["94"] = "Val de Marne (94)";
        $depts["95"] = "Val d'Oise (95)";
        $depts["97"] = "DOM (97)";
        return $depts;
    }


}