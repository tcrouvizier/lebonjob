<?php

namespace App\Controller;

use App\Repository\CandidateRepository;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="front_home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
            'departments' => $em->getRepository('App:Department')->findAll()
        ]);
    }

    /**
     * @Route("/list-page", name="front_list_page")
     */
    public function listPage()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/list.html.twig', [
            'controller_name' => 'FrontController'
        ]);
    }

    /**
     * @Route("/empty-page", name="front_empty_page")
     */
    public function emptyPage()
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/empty.html.twig', [
            'controller_name' => 'FrontController'
        ]);
    }

    /**
     * @Route("/departement/{id}", name="departement_job_page")
     */
    public function departementPage(OfferRepository $repository, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/departement.html.twig', [
            'controller_name' => 'FrontController',
            'jobDepart' => $repository->getJobParDepartement($id)
        ]);
    }

    /**
     * @Route("/offre/{id}", name="offre_description")
     */
    public function offre_description_page(OfferRepository $repository, int $id)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/offre.html.twig', [
            'controller_name' => 'FrontController',
            'offre' => $repository->getdetailOffre($id)
        ]);
    }

    /**
     * @Route("/offre/{id}/candidater", name="offre_candidate" )
     */
    public function offre_candidater(CandidateRepository $repository, $id)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/offre_candidater.html.twig', [
            'controller_name' => 'FrontController',
            'id' => $id,
        ]);
    }

    /**
     * @Route("/offre/candidater/new", name="offre_candidate_add" )
     */
    public function ajoutCandidat(CandidateRepository $repository)
    {
        if ($repository->getNbCandidaturePourOffre($_GET['id'], $_GET['email'])['nb'] == 0) {
            $repository->insertCandidat($_GET['id'], $_GET['nom'], $_GET['prenom'], $_GET['dateNaissance'], $_GET['email']);

        }

        return $this->redirectToRoute('front_home');
    }

    /**
     * @Route("/entreprise/ajouter-offre", name="Ajouter_une_offre")
     */
    public function ajoutOffreForm(OfferRepository $repository)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('front/ajoutOffre.html.twig', [
            'controller_name' => 'FrontController',
            'jobs' => $repository->getJobs(),
            'departements' => $repository->getDepartements()
        ]);
    }

    /**
     * @Route("/entreprise/ajouter-offre/new", name="offre_ajout" )
     */
    public function ajoutOffre(OfferRepository $repository)
    {
        $repository->insertOffer($_GET['job'],$_GET['departement'],$_GET['description'],$_GET['entreprise'],$_GET['email']);

        return $this->redirectToRoute('front_home');
    }
}
