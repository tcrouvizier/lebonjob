<?php

namespace App\Repository;

use App\Entity\Candidate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Candidate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Candidate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Candidate[]    findAll()
 * @method Candidate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Candidate::class);
    }

    public function insertCandidat($id, $nom, $prenom, $dateNaiss, $email)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO CANDIDATE (offer_candidate_id, nom, prenom, date_de_naissance, email, date_candidature) VALUES (?,?,?,?,?, NOW())';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id, $nom, $prenom, $dateNaiss, $email]);
    }

    public function getNbCandidaturePourOffre($id, $email) : array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT COUNT(id) as nb FROM CANDIDATE WHERE offer_candidate_id = ? AND email= ?';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id, $email]);

        return $stmt->fetch();
    }
}
