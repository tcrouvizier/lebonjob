<?php

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    public function getJobParDepartement($id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT d.name as depart_name, j.id, j.name, o.created_at, o.description, o.latitude, o.longitude FROM offer o INNER JOIN JOB j ON o.job_id=j.id INNER JOIN department d ON d.id=o.department_id WHERE o.department_id= ?';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);

        return $stmt->fetchAll();
    }

    public function getDetailOffre($id) : array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT d.name as depart_name, o.department_id, j.id, j.name, o.created_at, o.description, o.latitude, o.longitude FROM offer o INNER JOIN JOB j ON o.job_id=j.id INNER JOIN department d ON d.id=o.department_id WHERE j.id= ?';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$id]);

        return $stmt->fetch();
    }

    public function getJobs(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM JOB';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getDepartements(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM Department';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getOffreSemaine(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT * FROM offer o INNER JOIN JOB j ON j.id = o.job_id WHERE sticky=1';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function insertOffer($job_id, $depart_id, $description, $nom, $email)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO Offer (job_id, department_id, created_at, description, zipcode, city, contact_email, reference, latitude, longitude) VALUES (?,?,NOW(),?,null,null, ?, ?, null, null)';

        $stmt = $conn->prepare($sql);
        $stmt->execute([$job_id, $depart_id, $description, $email, $nom]);
    }
}
